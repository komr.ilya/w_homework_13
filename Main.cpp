#include <iostream>
#include "Math.h"

#define PI 3.14
//#define LOG

int main()
{
#ifdef LOG
	std::cout << "log messages!\n" << std::endl;
#endif // LOG

	int result = sum(17, 22);
	std::cout << "Result = " << result << std::endl;

	std::cout << "PI = " << PI << std::endl;
}